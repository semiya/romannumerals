public class RomanNumerals {
    public static String convert(Integer numbers) {
        return recursiveConvert(numbers);
    }

    private static String recursiveConvert(Integer numbers) {
        StringBuilder romanNumber = new StringBuilder("");

        if (numbers > 39) {
            romanNumber.append("L");
            numbers -= 50;
            if(numbers > 39) {
                return romanNumber.append(recursiveConvert(numbers)).toString();
            }
        }
        if (Math.abs(numbers) > 8) {
            if(numbers > 0) {
                romanNumber.append("X");
                numbers -= 10;
            }else {
                romanNumber.insert(romanNumber.length()-1, "X");
                numbers += 10;
            }

            if(numbers > 8) {
                return romanNumber.append(recursiveConvert(numbers)).toString();
            }
        }

        if (numbers > 3) {
            romanNumber.append("V");
            numbers -= 5;
        }

        for (int i =  0; i < Math.abs(numbers); i++) {
            if (numbers < 0)
                romanNumber.insert(romanNumber.length()-1, "I");
            else
                romanNumber.append("I");
        }


        return romanNumber.toString();
    }
}
