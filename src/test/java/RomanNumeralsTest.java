
import org.assertj.core.api.Assertions;
import org.junit.Ignore;
import org.junit.Test;

public class RomanNumeralsTest {
    @Test
    public void should_return_I_when_1() {
        RomanNumerals romanNumerals = new RomanNumerals();
        Integer numbers = 1;
        Assertions.assertThat(romanNumerals.convert(numbers)).isEqualTo("I");
    }

    @Test
    public void should_return_V_when_5() {
        RomanNumerals romanNumerals = new RomanNumerals();
        Integer numbers = 5;
        Assertions.assertThat(romanNumerals.convert(numbers)).isEqualTo("V");
    }

    @Test
    public void should_return_X_when_10() {
        RomanNumerals romanNumerals = new RomanNumerals();
        Integer numbers = 10;
        Assertions.assertThat(romanNumerals.convert(numbers)).isEqualTo("X");
    }

    @Test
    public void should_return_II_when_2() {
        RomanNumerals romanNumerals = new RomanNumerals();
        Integer numbers = 2;
        Assertions.assertThat(romanNumerals.convert(numbers)).isEqualTo("II");
    }

    @Test
    public void should_return_III_when_3() {
        RomanNumerals romanNumerals = new RomanNumerals();
        Integer numbers = 3;
        Assertions.assertThat(romanNumerals.convert(numbers)).isEqualTo("III");
    }

    @Test
    public void should_return_IV_when_4() {
        RomanNumerals romanNumerals = new RomanNumerals();
        Integer numbers = 4;
        Assertions.assertThat(romanNumerals.convert(numbers)).isEqualTo("IV");
    }

    @Test
    public void should_return_VI_when_6() {
        RomanNumerals romanNumerals = new RomanNumerals();
        Integer numbers = 6;
        Assertions.assertThat(romanNumerals.convert(numbers)).isEqualTo("VI");
    }

    @Test
    public void should_return_VII_when_7() {
        RomanNumerals romanNumerals = new RomanNumerals();
        Integer numbers = 7;
        Assertions.assertThat(romanNumerals.convert(numbers)).isEqualTo("VII");
    }

    @Test
    public void should_return_VIII_when_8() {
        RomanNumerals romanNumerals = new RomanNumerals();
        Integer numbers = 8;
        Assertions.assertThat(romanNumerals.convert(numbers)).isEqualTo("VIII");
    }

    @Test
    public void should_return_IX_when_9() {
        RomanNumerals romanNumerals = new RomanNumerals();
        Integer numbers = 9;
        Assertions.assertThat(romanNumerals.convert(numbers)).isEqualTo("IX");
    }

    @Test
    public void should_return_XIV_when_14() {
        RomanNumerals romanNumerals = new RomanNumerals();
        Integer numbers = 14;
        Assertions.assertThat(romanNumerals.convert(numbers)).isEqualTo("XIV");
    }
    @Test
    public void should_return_XVIII_when_18() {
        RomanNumerals romanNumerals = new RomanNumerals();
        Integer numbers = 18;
        Assertions.assertThat(romanNumerals.convert(numbers)).isEqualTo("XVIII");
    }

    @Test
    public void should_return_XIX_when_19() {
        RomanNumerals romanNumerals = new RomanNumerals();
        Integer numbers = 19;
        Assertions.assertThat(romanNumerals.convert(numbers)).isEqualTo("XIX");
    }
    @Test
    public void should_return_XL_when_40() {
        RomanNumerals romanNumerals = new RomanNumerals();
        Integer numbers = 40;
        Assertions.assertThat(romanNumerals.convert(numbers)).isEqualTo("XL");
    }

}



